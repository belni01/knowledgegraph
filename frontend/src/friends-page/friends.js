import {getFriendsData} from "../api/safetySphereApi";
import {Link, useLoaderData} from "react-router-dom";

export async function loader({params}) {
    const friends = await getFriendsData(params.profileId);
    return {friends};
}

export function Friends() {
    const {friends} = useLoaderData()

    return (
        <div id="friends">
            <div className={"row"}>
                <div>
                    <h1>Friends of Friends:</h1>
                    {friends.friendsOfFriends.length ? (
                        <ul className={"profile-list"}>
                            {friends.friendsOfFriends.map((friend) => (
                                <li key={friend.id} className={"profile-link"}>
                                    <Link to={`/friends-followers/friends/${friend.id}`}>
                                        {friend.alreadyFriends ? (
                                            <i>{friend.fullname} (Already Friends)</i>
                                        ) : (
                                            <i>{friend.fullname}</i>
                                        )}
                                    </Link>
                                </li>
                            ))}
                        </ul>
                    ) : (
                        <p>
                            <i>Friends have no friends</i>
                        </p>
                    )}
                </div>
                <div>
                    <h1>Recommended Path Friends:</h1>
                    {friends.recommendedFriends.length ? (
                        <ul className={"profile-list"}>
                            {friends.recommendedFriends.map((friend) => (
                                <li key={friend.id} className={"profile-link"}>
                                    <Link to={`/friends-followers/friends/${friend.id}`}>
                                        <i>{friend.fullname}: over {friend.length} friends</i>
                                    </Link>
                                </li>
                            ))}
                        </ul>
                    ) : (
                        <p>
                            <i>No recommended Friends</i>
                        </p>
                    )}
                </div>
            </div>
        </div>
    )
}