const wrapWithResCheck = fn => res => {
    if (!res.ok) {
        throw new Error(
            `Something went wrong. Received response: ${res.status} ${res.statusText}`
        );
    }
    return fn(res);
};

module.exports = {
    wrapWithResCheck,
};