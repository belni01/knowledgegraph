const { Connection } = require("stardog");

const conn = new Connection({
    username: "<YOUR_USERNAME>",
    password: "<YOUR_PASSWORD>",
    endpoint: "<YOUR_ENDPOINT>"
});

// An "enum" for the status of our request to Stardog for data.
const TableDataAvailabilityStatus = {
    NOT_REQUESTED: 'NOT_REQUESTED',
    LOADING: "LOADING",
    LOADED: "LOADED",
    FAILED: "FAILED"
};

module.exports = {
    dbName: 'safetySphere',
    conn,
    TableDataAvailabilityStatus,
};