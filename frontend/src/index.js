import React from 'react';
import ReactDOM from 'react-dom/client';
import reportWebVitals from './reportWebVitals';
import {createBrowserRouter, RouterProvider} from "react-router-dom";
import './css/App.css';
import ErrorPage from "./root/error-page";
import Profile, {loader as profileLoader, sqlProfileLoader} from "./profile-page/profile";
import People, {loader as rootLoader, sqlLoader} from "./root/people";
import App from "./App";
import {Friends, loader as friendsLoader} from "./friends-page/friends";
import {Posts, loader as postLoader} from "./post-page/post";
import CompanyProfile, {loader as companyProfileLoader} from "./company-page/company";
import Companies, {loader as companyRootLoader} from "./root/companies";


const router = createBrowserRouter([
    {
        path: "/",
        element: <App />,
        children: [
            {
                path: "people",
                element: <People link={"profile"}/>,
                errorElement: <ErrorPage/>,
                loader: rootLoader,
                children: [
                    {
                        path: "profile/:profileId",
                        element: <Profile path={"/people/profile"}/>,
                        loader: profileLoader
                    }
                ]
            },
            {
                path: "people-sql",
                element: <People link={"sql"}/>,
                errorElement: <ErrorPage/>,
                loader: sqlLoader,
                children: [
                    {
                        path: "sql/:profileId",
                        element: <Profile path={"/people-sql/sql"}/>,
                        loader: sqlProfileLoader
                    }
                ]
            },
            {
                path: "friends-followers",
                element: <People link={"friends"}/>,
                errorElement: <ErrorPage/>,
                loader: rootLoader,
                children: [
                    {
                        path: "friends/:profileId",
                        element: <Friends/>,
                        loader: friendsLoader
                    }
                ]
            },
            {
                path: "posts",
                element: <People link={"post"}/>,
                loader: rootLoader,
                children: [
                    {
                        path: "post/:userId",
                        element: <Posts/>,
                        loader: postLoader
                    }
                ]
            },
            {
                path: "companies",
                element: <Companies link={"company"}/>,
                loader: companyRootLoader,
                children: [
                    {
                        path: "company/:companyId",
                        element: <CompanyProfile/>,
                        loader: companyProfileLoader
                    }
                ]
            }
        ]
    },
]);

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <React.StrictMode>
        <RouterProvider router={router} />
    </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
