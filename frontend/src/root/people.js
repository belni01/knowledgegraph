import {Link, Outlet, useLoaderData} from "react-router-dom";
import {getPeople, getPeopleFromSQL} from "../api/safetySphereApi";

export async function loader() {
    const people = await getPeople();
    return {people};
}

export async function sqlLoader() {
    const people = await getPeopleFromSQL()
    return {people};
}

export default function People(props) {
    const {people} = useLoaderData()
    return (
        <>
            <div id="sidebar">
                <nav>
                    {people.length ? (
                        <ul>
                            {people.map((person) => (
                                <li key={person.id}>
                                    <Link to={`${props.link}/${person.id}`}>
                                        <i>{person.fullname}</i>
                                    </Link>
                                </li>
                            ))}
                        </ul>
                    ) : (
                        <p>
                            <i>No contacts</i>
                        </p>
                    )}
                </nav>
            </div>
            <div className={"detail"}>
                <Outlet />
            </div>
        </>
    );
}