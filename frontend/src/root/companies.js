import {Link, Outlet, useLoaderData} from "react-router-dom";
import {getCompanies} from "../api/safetySphereApi";

export async function loader() {
    const companies = await getCompanies();
    return {companies}
}

export default function Companies(props) {
    const {companies} = useLoaderData()
    return (
        <>
            <div id="sidebar">
                <nav>
                    {companies.length ? (
                        <ul>
                            {companies.map((company) => (
                                <li key={company.id}>
                                    <Link to={`${props.link}/${company.id}`}>
                                        <i>{company.name}</i>
                                    </Link>
                                </li>
                            ))}
                        </ul>
                    ) : (
                        <p>
                            <i>No Companies</i>
                        </p>
                    )}
                </nav>
            </div>
            <div className={"detail"}>
                <Outlet/>
            </div>
        </>
    );
}