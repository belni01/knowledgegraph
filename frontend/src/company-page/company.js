import {Link, useLoaderData} from 'react-router-dom';
import {getCompanyProfile} from "../api/safetySphereApi";

export async function loader({ params }) {
    const companyProfile = await getCompanyProfile(params.companyId);
    return {companyProfile };
}

function CompanyProfile() {
    const { companyProfile } = useLoaderData();

    return (
        <div id="profile">
            <div>
                <h1>{companyProfile.name}</h1>
                <p>City: {companyProfile.city}</p>
            </div>
            <div className={"row"}>
                <div>
                    <h2>Followers:</h2>
                    {companyProfile.followers.length ? (
                        <ul className={"profile-list"}>
                            {companyProfile.followers.map((person) => (
                                <li key={person.id} className={"profile-link"}>
                                    <Link to={`/people/profile/${person.id}`}>
                                        <i>{person.fullname}</i>
                                    </Link>
                                </li>
                            ))}
                        </ul>
                    ) : (
                        <p><i>No followers</i></p>
                    )}
                </div>
                <div>
                    <h2>Cities of Followers:</h2>
                    {companyProfile.cityOfFollower && companyProfile.cityOfFollower.length ? (
                        <ul className={"profile-list"}>
                            {companyProfile.cityOfFollower.map((city) => (
                                <li key={city.city} className={"profile-link"}>
                                    {city.count >= 3 ? (
                                        <i>{city.count} followers are from {city.city}</i>
                                    ) : (
                                        <i>
                                            {city.names.join(', ')} {city.count === 1 ? 'is' : 'are'} from {city.city}
                                        </i>
                                    )}
                                </li>
                            ))}
                        </ul>
                    ) : (
                        <p><i>No follower data by city</i></p>
                    )}
                </div>
            </div>
        </div>
    );
}

export default CompanyProfile;