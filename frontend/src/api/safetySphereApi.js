import {query, virtualGraphs} from "stardog";
import {conn, dbName} from "../helpers/constants";

export async function getPeople() {
    let result = []
    // language=SPARQL
    let readQuery = `
        PREFIX : <tag:stardog:designer:SafetySphere:model:>
        SELECT * {
            ?person a :Person ;
                    rdfs:label ?fullname
        }
    `

    await query.execute(conn, dbName, readQuery)
        .then(res => {
            if (!res.ok) {
                console.log("Error")
                return;
            }

            res.body.results.bindings.forEach(binding => {
                result.push({
                    fullname: binding.fullname.value,
                    iri: binding.person.value,
                    id: binding.person.value.match(/\d+$/)[0],
                    from: "Stardog"
                })
            })
        });

    return result;
}

export async function getPeopleFromSQL() {
    let result = []
    // language=SPARQL
    let readQuery = `
    PREFIX : <tag:stardog:designer:SafetySphere:model:>
    
    SELECT ?person ?fullname ?from {
        {
            GRAPH <tag:stardog:api:context:default> {
                ?person a :Person ;
                        rdfs:label ?fullname
                BIND('Stardog' as ?from)
            }
        }
        UNION
        {
            GRAPH <virtual://azure-stardog> {
                ?person a :Person ;
                        :firstname ?firstname ;
                        :lastname ?lastname .
                BIND(CONCAT(?firstname, CONCAT(" ", ?lastname)) as ?fullname)
                BIND('SQL' as ?from)
            }
        }
    }
    `

    await query.execute(conn, dbName, readQuery)
        .then(res => {
            if (!res.ok) {
                console.log("Error")
                return
            }

            res.body.results.bindings.forEach(binding => {
                result.push({
                    fullname: binding.fullname.value,
                    iri: binding.person.value,
                    id: binding.person.value.match(/\d+$/)[0],
                    from: binding.from.value
                })
            })
        })

    return result
}

export async function getCompanies() {
    let result = []
    // language=SPARQL
    let readQuery = `
        PREFIX : <tag:stardog:designer:SafetySphere:model:>
        SELECT * {
            ?company a :Company ;
                :name ?name
            }
    `

    await query.execute(conn, dbName, readQuery)
        .then(res => {
            if (!res.ok) {
                console.log("Error")
                return;
            }

            res.body.results.bindings.forEach(binding => {
                result.push({
                    name: binding.name.value,
                    iri: binding.company.value,
                    id: binding.company.value.match(/\d+$/)[0]
                })
            })
        });

    return result;
}


export async function getFollows(id) {
    let result = []
    // language=SPARQL
    let readQuery = `
        PREFIX : <tag:stardog:designer:SafetySphere:model:>

        SELECT ?follower ?fullname ?is_company {
            ?person a :Person ;
                    :follows ?follower .
            ?follower rdfs:label ?fullname .
            ?follower a ?type
            FILTER(?person = <http://safetynet.com/safetysphere/user/${id}>)
            BIND(IF(?type = :Company, true, false) as ?is_company)
        }
    `

    await query.execute(conn, dbName, readQuery)
        .then(res => {
            if (!res.ok) {
                console.log("Error")
                return;
            }

            res.body.results.bindings.forEach(binding => {
                result.push({
                    fullname: binding.fullname.value,
                    id: binding.follower.value.match(/\d+$/)[0],
                    isCompany: binding.is_company.value === "true"
                })
            })
        })

    return result
}

export async function getFollowers(id) {
    let result = []
    // language=SPARQL
    let readQuery = `
        PREFIX : <tag:stardog:designer:SafetySphere:model:>
        
        SELECT ?follower ?fullname ?city {
            ?company a :Company ;
                     ^:follows ?follower .
            ?follower rdfs:label ?fullname;
                      a :User ;
                      :city ?city
            FILTER(?company = <http://safetynet.com/safetysphere/user/${id}>)
        }
    `

    await query.execute(conn, dbName, readQuery, "application/sparql-results+json", {reasoning: true})
        .then(res => {
            if (!res.ok) {
                console.log("Error")
                return;
            }

            res.body.results.bindings.forEach(binding => {
                result.push({
                    fullname: binding.fullname.value,
                    city: binding.city.value,
                    id: binding.follower.value.match(/\d+$/)[0]
                })
            })
        })

    return result
}

export async function getFriends(id, reasoning) {
    if (reasoning === undefined) {
        reasoning = false
    }

    let result = []
    // language=SPARQL
    let readQuery = `
        PREFIX : <tag:stardog:designer:SafetySphere:model:>
    
        SELECT ?friend ?fullname {
            ?person a :Person ;
                    :is_friend ?friend .
            ?friend rdfs:label ?fullname
            FILTER (?person = <http://safetynet.com/safetysphere/user/${id}>)
        }
    `

    await query.execute(conn, dbName, readQuery, "application/sparql-results+json", {reasoning: reasoning})
        .then(res => {
            if (!res.ok) {
                console.log("Error")
                return
            }

            res.body.results.bindings.forEach(binding => {
                result.push({
                    fullname: binding.fullname.value,
                    id: binding.friend.value.match(/\d+$/)[0]
                })
            })
        })

    return result
}

export async function getProfile(id, sql) {
    let named = ""
    if (sql !== undefined) {
        named = `
            FROM <tag:stardog:api:context:default>
            FROM <virtual://azure-stardog>
        `
    }

    let result = []
    // language=SPARQL
    let readQuery = `
        PREFIX : <tag:stardog:designer:SafetySphere:model:>

        SELECT ?firstname ?lastname ?city
        ${named}
        {
            ?person a :Person ;
                    :firstname ?firstname ;
                    :lastname ?lastname ;
                    :city ?city .
            FILTER (?person = <http://safetynet.com/safetysphere/user/${id}>)
        }
    `

    let profile = query.execute(conn, dbName, readQuery)
        .then(res => {
            if (!res.ok) {
                console.log("Error")
                return;
            }

            res.body.results.bindings.forEach(binding => {
                result.push({
                    fullname: `${binding.firstname.value} ${binding.lastname.value}`,
                    firstname: binding.firstname.value,
                    lastname: binding.lastname.value,
                    city: binding.city.value,
                })
            })

            result = result[0]
        });

    let [follows, realFriends, inducedFriends] = await Promise.allSettled([
        getFollows(id),
        getFriends(id),
        getFriends(id, true),
        profile
    ]);

    result.follows = follows.value
    result.realFriends = realFriends.value
    result.inducedFriends = inducedFriends.value

    return result;
}

export async function getCityOfFollower(id) {
    let result = [];
    // language=SPARQL
    let readQuery = `
        PREFIX : <tag:stardog:designer:SafetySphere:model:>

        SELECT ?city (GROUP_CONCAT(DISTINCT ?fullname; separator=", ") AS ?names) (COUNT(?follower) AS ?count) {
            ?company a :Company ;
                    ^:follows ?follower .
            ?follower a :Person ;
                      :city ?city ;
                      rdfs:label ?fullname .
            FILTER(?company = <http://safetynet.com/safetysphere/user/${id}>)
        }
        GROUP BY ?city
    `;

    await query.execute(conn, dbName, readQuery, "application/sparql-results+json", {reasoning: true})
        .then(res => {
            if (!res.ok) {
                console.log("Error");
                return;
            }

            res.body.results.bindings.forEach(binding => {
                result.push({
                    city: binding.city.value,
                    names: binding.names.value.split(", "),
                    count: parseInt(binding.count.value, 10)
                });
            });
        });

    return result;
}

export async function getFriendsOfFriends(id) {
    let result = []
    // language=SPARQL
    let readQuery = `
        PREFIX : <tag:stardog:designer:SafetySphere:model:>
        PREFIX user: <http://safetynet.com/safetysphere/user/>
            
        SELECT ?friendOfFriend ?fullname (MAX(?alreadyFriends) AS ?alreadyFriendsValue) {
            ?person a :Person ;
                    :is_friend ?friend .
            ?friend :is_friend ?friendOfFriend .
            ?friendOfFriend rdfs:label ?fullname .
            {
                SELECT ?f {
                    ?p a :Person ;
                       :is_friend ?f
                    FILTER (?p = user:${id})
                }
            }
            BIND(IF(?friendOfFriend in (?f) , 1, 0) as ?alreadyFriends)
            FILTER(?person = user:${id} && ?person != ?friendOfFriend)
        } GROUP BY ?friendOfFriend ?fullname
        ORDER BY ?fullname
    `

    await query.execute(conn, dbName, readQuery, "application/sparql-results+json", {reasoning: true})
        .then(res => {
            if (!res.ok) {
                console.log("Error")
                return
            }

            res.body.results.bindings.forEach(binding => {
                result.push({
                    fullname: binding.fullname.value,
                    id: binding.friendOfFriend.value.match(/\d+$/)[0],
                    alreadyFriends: binding.alreadyFriendsValue.value === "1"
                })
            })
        })

    return result
}

export async function getRecommendedFriends(id) {
    let result = []
    let ids = []
    let people = getPeople()
    // language=SPARQL
    let readQuery = `
        PREFIX : <tag:stardog:designer:SafetySphere:model:>
        PREFIX user: <http://safetynet.com/safetysphere/user/>
        
        PATHS SHORTEST
            START ?x = user:${id}
            END ?y
        VIA {
            ?x :is_friend ?y
            FILTER (?y != user:${id})
        }
        MAX LENGTH 3 
    `

    await query.execute(conn, dbName, readQuery, "application/sparql-results+json", {reasoning: true})
        .then(res => {
            if (!res.ok) {
                console.log("Error")
                return
            }

            let prev = {}
            let length = 0

            res.body.results.bindings.forEach((binding, index, array) => {
                if (Object.keys(binding).length === 0 || index === array.length - 1) {
                    let id = prev.y.value.match(/\d+$/)[0]
                    if (!ids.includes(id) && length > 2) {
                        result.push({
                            id: id,
                            length: length - 2
                        })

                        ids.push(id)
                    }

                    length = 0
                }

                prev = binding
                length++
            })

            people.then(people => {
                // console.log(people)
                result.forEach(value => {
                    value.fullname = people.find(p => p.id === value.id).fullname
                })
            })

        })

    return result
}

export async function getFriendsData(id) {
    let result = {}
    let friendsOfFriends = getFriendsOfFriends(id)
    let recommendedFriends = getRecommendedFriends(id)

    let [fof, rf] = await Promise.allSettled([friendsOfFriends, recommendedFriends])

    result.friendsOfFriends = fof.value
    result.recommendedFriends = rf.value

    return result
}
export async function getCompanyProfile(id)  {
    let result = []
    // language=SPARQL
    let readQuery = `
        PREFIX : <tag:stardog:designer:SafetySphere:model:>
        SELECT * {
            ?company a :Company ;
                     :name ?name ;
                     :city ?city .
           FILTER (?company = <http://safetynet.com/safetysphere/user/${id}>)  
        }
    `

    await query.execute(conn, dbName, readQuery)
        .then(res => {
            if (!res.ok) {
                console.log("Error")
                return;
            }

            res.body.results.bindings.forEach(binding => {
                result.push({
                    name: binding.name.value,
                    city: binding.city.value,
                })
            })

            result = result[0]
        });

    let [followers, citiesOfFollowers] = await Promise.allSettled([
        getFollowers(id),
        getCityOfFollower(id)
    ]);

    result.followers = followers.value
    result.cityOfFollower= citiesOfFollowers.value

    return result;
}

export async function getPostInfo(id){
    let result = []
    let readQuery = `
        PREFIX : <tag:stardog:designer:SafetySphere:model:>

        SELECT ?post ?title ?timestamp ?content ?fullname {
            ?post a :Post ;
                    :title ?title ;
                    :timestamp ?timestamp ;
                    :content ?content ;
                    :created_by ?creater .
            FILTER (?creater = <http://safetynet.com/safetysphere/user/${id}>)
            {
                ?post a :Post ;
                        ^:likes ?liker ;
                        :created_by ?creater .
                ?liker rdfs:label ?fullname
            }  
        }
        ORDER BY ?post ?title
    `

    await query.execute(conn, dbName, readQuery)
        .then(res => {
            if (!res.ok) {
                console.log("Error")
                return
            }

            let prev = ""
            let post = {}
            post.liker = []

            res.body.results.bindings.forEach(binding => {
                if (binding.post.value !== prev) {
                    post = {
                        title: binding.title.value,
                        timestamp: binding.timestamp.value,
                        content: binding.content.value,
                        id: binding.post.value.match(/\d+$/),
                        liker: [{
                            fullname: binding.fullname.value,
                        }]
                    }
                    result.push(post)
                } else {
                    post.liker.push({
                        fullname: binding.fullname.value,
                    })
                }
                prev = binding.post.value
            })
        });

    return result
}