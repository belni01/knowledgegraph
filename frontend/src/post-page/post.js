import {getPostInfo} from "../api/safetySphereApi";
import {Link, useLoaderData} from "react-router-dom";

export async function loader({params}){
    const posts = await getPostInfo(params.userId);
    return { posts };
}

export function Posts(props) {
    const { posts } = useLoaderData();
    console.log(posts);


    return (
        <div id="friends">
            {posts.length ? (
                posts.map((post) => (
                    <div key={post.id}>
                        <h1>{post.title}</h1>
                        <span>
                            <p>Time: {post.timestamp} </p>
                        </span>
                        <div className={"row"}>
                            <div>
                                <p>{post.content}</p>
                            </div>

                            {post.liker.length ? (
                                <div>
                                    <p>Liked by:</p>
                                    <ul className={"profile-link"}>
                                        {post.liker.map((liker) => (
                                            <li>
                                                {liker.fullname}
                                            </li>
                                        ))}
                                    </ul>
                                </div>
                            ) : (
                                <p>No likes</p>
                            )}

                        </div>
                    </div>
                ))
            ) : (
                <p>No posts created</p>
            )}
        </div>
    );
}


