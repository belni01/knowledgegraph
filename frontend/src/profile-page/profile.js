import {Link, useLoaderData} from "react-router-dom";
import {getProfile} from "../api/safetySphereApi";

export async function loader({ params }) {
    const profile = await getProfile(params.profileId);
    return { profile };
}

export async function sqlProfileLoader({ params }) {
    const profile = await getProfile(params.profileId, true);
    return { profile };
}

function Profile(params) {
    const { profile } = useLoaderData();

    return (
        <div id="profile">
            <div>
                <h1>{profile.fullname}</h1>
                <span>
                    <p>Firstname: {profile.firstname}</p>
                </span>
                <span>
                    <p>Lastname: {profile.lastname}</p>
                </span>
                <span>
                    <p>City: {profile.city}</p>
                </span>
            </div>
            <div className={"row"}>
                <div>
                    <h1>Follows:</h1>
                    {profile.follows.length ? (
                        <ul className={"profile-list"}>
                            {profile.follows.map((person) => (
                                <li key={person.id} className={"profile-link"}>
                                    {person.isCompany ? (
                                        <div>
                                            <i>{person.fullname} (Company)</i>
                                        </div>
                                    ) : (
                                        <Link to={`${params.path}/${person.id}`}>
                                            <i>{person.fullname}</i>
                                        </Link>
                                    )}
                                </li>
                            ))}
                        </ul>
                    ) : (
                        <p>
                            <i>Follows no one</i>
                        </p>
                    )}
                </div>
                <div>
                    <h1>Friends:</h1>
                    {profile.realFriends.length ? (
                        <ul className={"profile-list"}>
                            {profile.realFriends.map((person) => (
                                <li key={person.id} className={"profile-link"}>
                                    <Link to={`${params.path}/${person.id}`}>
                                        <i>{person.fullname}</i>
                                    </Link>
                                </li>
                            ))}
                        </ul>
                    ) : (
                        <p>
                            <i>No Friends</i>
                        </p>
                    )}
                </div>
                <div>
                    <h1>Induced Friends:</h1>
                    {profile.inducedFriends.length ? (
                        <ul className={"profile-list"}>
                            {profile.inducedFriends.map((person) => (
                                <li key={person.id} className={"profile-link"}>
                                    <Link to={`${params.path}/${person.id}`}>
                                        <i>{person.fullname}</i>
                                    </Link>
                                </li>
                            ))}
                        </ul>
                    ) : (
                        <p>
                            <i>No induced Friends</i>
                        </p>
                    )}
                </div>
            </div>
        </div>
    );
}

export default Profile;