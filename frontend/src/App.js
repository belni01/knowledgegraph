import logo from './logo.svg';
import {Link, Outlet} from "react-router-dom";

function App() {
  return (
    <div className="App">
      <header className="App-header">
          <ul className={"navigation-list"}>
              <li className={"nav-item"}><Link to={"people"}>People</Link></li>
              <li className={"nav-item"}><Link to={"people-sql"}>People SQL</Link></li>
              <li className={"nav-item"}><Link to={"friends-followers"}>Friends & Followers</Link></li>
              <li className={"nav-item"}><Link to={"posts"}>Posts</Link></li>
              <li className={"nav-item"}><Link to={"companies"}>Company</Link></li>
          </ul>
      </header>
        <div id={"content"}>
            <Outlet/>
        </div>
    </div>
  );
}

export default App;
