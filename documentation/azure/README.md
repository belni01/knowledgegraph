# Azure SQL guide

## Introduction

In this guide, you will learn, how to create an SQL database on Azure.

## Create database

1. Open in the Azure portal
   the [SQL databases](https://portal.azure.com/#view/HubsExtension/BrowseResource/resourceType/Microsoft.Sql%2Fservers%2Fdatabases)
   section.
2. Click on **Create**.
3. You can select any resource group and subscription.
4. Set the database name to **safetySphere**.
5. Click on **Create new**, in the server section.
6. You can use any location and name.
7. Select **Use SQL authentication** in the *Authentication method* section.
8. Set any username and password. Note both values down, as you will need them later in the setup.
9. Click on **OK**.
10. Click on **Configure database** in the *Compute + storage* section.
11. Select **Basic** in the *Service tier* section.
12. Click on **Apply**.
13. Select **Locally-redundant backup storage** in the *Backup storage redundancy* section.
14. Click on **Review + create**.
15. Click on **Create**.
16. Wait for the deployment to finish.

## Set firewall rule

1. Open in the Azure portal
   the [SQL servers](https://portal.azure.com/#view/HubsExtension/BrowseResource/resourceType/Microsoft.Sql%2Fservers)
   section.
2. Select your newly created SQL server.
3. Select **Networking** in the *Security* section.
4. If the public network access is set to disabled, select **Selected networks**.
5. Click on **Add a firewall rule** in the *Firewall rules* section.
6. Set the *Rule name* to **All**, *Start IP* to **0.0.0.0** and *End IP* to **255.255.255.255**.
7. Click on **OK**.
8. Click on **Save**.

## Get the JDBC connection string

1. Open in the Azure portal
   the [SQL databases](https://portal.azure.com/#view/HubsExtension/BrowseResource/resourceType/Microsoft.Sql%2Fservers%2Fdatabases)
   section.
2. Select your newly created database.
3. Click on **Connection strings**.
4. Click on **JDBC**.
5. Copy part of the string like this:
   jdbc:sqlserver://<YOUR_SERVER>:1433;database=safetySphere;
6. Note this, you will need it later in the installation

## Conclusion

Now you can continue the setup in the [setup guide](../setup/README.md).