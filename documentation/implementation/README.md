# Our Implementation

## Introduction

In this guide, you will learn how this project functions, with a focus on the SPARQL queries which the frontend
executes, to get data from stardog. This project is about a social media platform called *SafetySphere*.

## Architecture

### Project Architecture

Here you can see the project architecture. There is a frontend application which was written in React.
It communicates with the stardog engine, which in turn communicates with an SQL database and the safetySphere graph.

![architecture](./images/architecture.png)

### Graph schema

Here you can see the schema of the safetySphere graph. There are people and companies. Both are subclasses of user.
Both can make posts and can follow each other or be friends.

![graph-schema](./images/graph-schema.png)

### SQL schema

The sql schema is held quiet easy. It is only used, to showcase the capabilities of virtual graphs.
If you want to learn what virtual graphs are, you can check out
the [knowledge graph documentation](../knowledge_graph/README.md).

![sql-schema](./images/sql-schema.png)

## Frontend

As already stated, the frontend was written in React. It uses the package *stardog*, to communicate with the Stardog
engine. This is done over normal http requests to the Stardog API.

There are five tabs in the application, and each of them showcases a different use case:

- People (Normal SPARQL Queries)
- People SQL (Virtual Graph connection / Union)
- Friends & Followers (Path Queries)
- Posts (Join)
- Company (Reverse relationship)

### Normal Sparql query

Sparql queries function in a very similar way to SQL queries. Here is an example of a normal sparql query:

```sparql
PREFIX : <tag:stardog:designer:SafetySphere:model:>

SELECT ?firstname ?lastname ?city
{
    ?person a :Person ;
            :firstname ?firstname ;
            :lastname ?lastname ;
            :city ?city .
    FILTER (?person = <http://safetynet.com/safetysphere/user/1>)
}
```

This query gets a Person with their attributes from the database.
The query selects three different variables: the firstname, lastname and the city.
In the first line of the select statement, you can see that the query wants all entries which are of type `:Person`.
You may have noticed that it doesn't explicitly ask for a `rdf:type`,
as this is very often used, there is a shortcut, you can write `a` and it is recognized as a `rdf:type`.

The prefix is defined in the first line. This way you don't have to write it in front of every relationship.
In this case, no name was given to the prefix, and it can therefore be used simply by doing a double point in front of
the relationship.

It is also possible to give a prefix a name. This is especially useful when you have multiple prefixes in one query.
If you want to name your prefix, you need to do it this
way: `PREFIX ex: <tag:stardog:designer:SafetySphere:model:>`.
Afterward it could be used like this: `?person a ex:Person`.

The query also performs a filter operation. It only wants the data from the person with the IRI of *.../user/1*.

One other very nice feature of sparql is that you can end one line of the where clause with a semicolon. This indicates
that you want to use the same subject on the next line. In fact you could write out ?person in every line. That would
make the query equivalent to the current one. This would look like this:

```sparql
PREFIX : <tag:stardog:designer:SafetySphere:model:>

SELECT ?firstname ?lastname ?city
{
    ?person a :Person .
    ?person :firstname ?firstname .
    ?person :lastname ?lastname .
    ?person :city ?city .
    FILTER (?person = <http://safetynet.com/safetysphere/user/1>)
}
```

Whenever you want to work with a different subject, you need to end the line in a dot. Here you can see an example
with different subjects:

```sparql
PREFIX : <tag:stardog:designer:SafetySphere:model:>
    
SELECT ?friend ?fullname {
    ?person a :Person ;
            :is_friend ?friend .
    ?friend rdfs:label ?fullname
    FILTER (?person = <http://safetynet.com/safetysphere/user/1>)
}
```

Here you first specify which friends you want to get and then that you want to have the `rdfs:label` of them, which
is in our case always the fullname of the person or the company name.

### Virtual Graph connection

To query data from a virtual graph, you first need to set up the connection and the mapping for the external datasource.
If you want to learn how to set up the connection, you can check out the [setup guide](../setup/README.md).
The mapping in stardog done in the sms (stardog mapping syntax) format. Here you can see the mapping for our Person
table in the SQL database.

```sms
PREFIX : <tag:stardog:designer:SafetySphere:model:>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX so: <https://schema.org/>
PREFIX stardog: <tag:stardog:api:>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

MAPPING
FROM SQL {
  SELECT *
  FROM [SafetySphere].[dbo].[Person]
}
TO {
  ?subject :city ?city .
  ?subject :firstname ?firstname .
  ?subject :id ?id_integer .
  ?subject :lastname ?lastname .
  ?subject rdf:type :Person
} WHERE {
  BIND(StrDt(?id, xsd:integer) AS ?id_integer)
  BIND(template("http://safetynet.com/safetysphere/user/{id}") AS ?subject)
}
```

First, you need to specify the prefix as usual, you can take any prefix, but if you take the same one as you have in
your normal graph, you can easily combine the results.

You need to start with the keyword `MAPPING`, you can follow it with an optional IRI for this mapping, which wasn't
done in this example. This is just used to name the mapping.

It is followed by the keywords `FROM SQL`. This depends on the source your data is coming from. In this case, it is
an SQL database. But it can also be `JSON`, `CSV` or `GraphQL`. As SQL was used here, you need to put the SQL query in
this section.

This is then followed by the keyword `TO`. Here you can specify the triplet pattern. Keep in mind, that you need to
use the variable names from the columns, which you SQL query returns. This means, that if the query returns a column
with the name `firstname`, you need to use `?firstname` to retrieve the information from this column.

The last section is annotated with the keyword `WHERE`. In this section you can transform the source data and use
`BIND`, to bind the transformed data. Here are two transformations done, the first one uses the `StrDt` function.
This function transforms a string, to a different datatype, in this case to an `xsd:integer` and binds it to a new
variable. The other transformation assigns an IRI, using the function `template`. You can use string interpolation
here, and you can insert any of your variables, keep in mind, that you don't need to prefix your variable with
a question mark.

If you want a more in depth explanation, you can visit
the [stardog documentation](https://docs.stardog.com/virtual-graphs/mapping-data-sources).

When the schema is defined, you can start to query the data using sparql queries. There are two ways to query the data,
both use named graphs and both were used in this project, as a showcase. Here you can see a first example:

```sparql
PREFIX : <tag:stardog:designer:SafetySphere:model:>

SELECT ?firstname ?lastname ?city
FROM <tag:stardog:api:context:default>
FROM <virtual://azure-stardog>
{
    ?person a :Person ;
            :firstname ?firstname ;
            :lastname ?lastname ;
            :city ?city .
    FILTER (?person = <http://safetynet.com/safetysphere/user/1>)
}
```

This query gets the data from the default graph as well as from the virtual graph `azure-stardog`. Stardog uses
the syntax `virtual://<VIRTUAL_GRAPH>` for all its virtual graph connections.

By using the keyword `FROM` after the `SELECT` keyword, you can specify from which named graph you want to get the
information.

Another approach would be to use the keyword `GRAPH` inside of the `WHERE` block. Here you can see an example:

```sparql
PREFIX : <tag:stardog:designer:SafetySphere:model:>

SELECT ?person ?fullname ?from {
    {
        GRAPH <tag:stardog:api:context:default> {
            ?person a :Person ;
                    rdfs:label ?fullname
            BIND('Stardog' as ?from)
        }
    }
    UNION
    {
        GRAPH <virtual://azure-stardog> {
            ?person a :Person ;
                    :firstname ?firstname ;
                    :lastname ?lastname .
            BIND(CONCAT(?firstname, CONCAT(" ", ?lastname)) as ?fullname)
            BIND('SQL' as ?from)
        }
    }
}
```

Both graphs get queried and the result gets combined with the keyword `UNION`. This query gets all the people from both
data sources.

### Path queries

Path queries are a special feature of stardog. They don't belong to the standard implementation of sparql.
They are used to get all possible paths from one point x to another point y, over specified relations.
Here you can see an example from this project:

```sparql
PREFIX : <tag:stardog:designer:SafetySphere:model:>
PREFIX user: <http://safetynet.com/safetysphere/user/>

PATHS SHORTEST
    START ?x = user:1
    END ?y
VIA {
    ?x :is_friend ?y
    FILTER (?y != user:1)
}
MAX LENGTH 3 
```

This query gets all friends of friends up to a maximum length of three.

Path queries start with the keyword `PATHS`, followed by the type of path you want, the types are: `SHORTEST`, `ALL` and
`CYCLIC`. `SHORTEST` is the default value and doesn't need to be written out, it just looks a bit cleaner with it.
It gets, as the name implies, all paths sorted in order by length starting with the smallest one.

After that you need to use the keywords `START` and `END`, followed by variable names. You can specify the start and
endpoint by using the equal operator, but this is not mandatory. In this case, only the start was specified, as the
query should get all paths from this point onward.

Then you need to use the `VIA` keyword. In this block, you can specify over which relation you want to get the paths.
In this case the query should get all paths, where `?x :is_friend ?y` and as you can see, you can also apply filters
in the VIA section. In this case, the filter was done to prevent cyclic connections.

The query end with the statement `MAX LENGHT 3`, this is done, to prevent the query to output all paths no matter
the length. It is not necessary to use it, it just made sense here.

If you want to learn more about path queries, you can check out the [official stardog documentation](https://docs.stardog.com/query-stardog/path-queries).

### Join

As in SQL, you can use subqueries and join the results of these two queries. This was also done in this project, to
showcase the feature.

```sparql
PREFIX : <tag:stardog:designer:SafetySphere:model:>

SELECT ?post ?title ?timestamp ?content ?fullname {
    ?post a :Post ;
            :title ?title ;
            :timestamp ?timestamp ;
            :content ?content ;
            :created_by ?creater .
    FILTER (?creater = <http://safetynet.com/safetysphere/user/1>)
    {
        ?post a :Post ;
                ^:likes ?liker ;
                :created_by ?creater .
        ?liker rdfs:label ?fullname
    }  
}
ORDER BY ?post ?title
```

This query gets in the first section all the data from a post which was written by a specific user. It is followed by
another block with a different query inside. The other query gets all users, which liked the specific post. This is
possible, because both queries use the same variable, in this case `?post`. The output will look like this:

| post                                       | title                    | timestamp              | content                                                                                 | fullname         |
|--------------------------------------------|--------------------------|------------------------|-----------------------------------------------------------------------------------------|------------------|
| http://safetynet.com/safetysphere/post/1	  | "Sunny Day in the Park"	 | "2024-04-23T08:00:00"	 | "Today is a sunny day, perfect for a stroll in the park."                               | 	"Carlos Contoh" |
| http://safetynet.com/safetysphere/post/1	  | "Sunny Day in the Park"	 | "2024-04-23T08:00:00"	 | "Today is a sunny day, perfect for a stroll in the park."                               | 	"Erik Ejemplo"  |
| http://safetynet.com/safetysphere/post/10	 | "Creative Break"	        | "2024-04-14T10:00:00"	 | "I finally took some time to get back into painting. It's so relaxing and fulfilling."	 | "Julia Jemplo"   |

As you can see, both results were joined together.

### Reverse relationships

Until this point mostly normal relationships were used like `?person :follows ?follows`. But it is very easy to get the
reverse relationship in sparql. You simply need to annotate the relationship with a `^`. This then turns the relationship
around.

If we take the previous example of `?person :follows ?follows` and we want instead of knowing who the person follows, to
get the followers of this person, we can turn the relationship around like this: `?person ^:follows ?follower`.
This was used a few times in this project, one example is this query:

```sparql
PREFIX : <tag:stardog:designer:SafetySphere:model:>

SELECT ?follower ?fullname ?city {
    ?person a :Company ;
            ^:follows ?follower .
    ?follower rdfs:label ?fullname;
              a :User ;
              :city ?city
    FILTER(?person = <http://safetynet.com/safetysphere/user/12>)
}
```

This query gets all followers of a company, including their names and cities.

## Reasoning

Stardog can use rules, to infer relationships between nodes. If you want to learn what reasoning is, you can check out
the [knowledge graph documentation](../knowledge_graph/README.md).

To create a rule in stardog, you need either to do it in code or in the Stardog Designer. In this project it was
done using the Designer.

When you open a project in the Stardog Designer, you can click on **Rules**. There you can create new rules or modify
existing ones. In this project, there is only one rule called *follower_to_friends*. It infers, if two users follow
each other, they are friends. Here you can see how the rule looks in the Designer:

![rule](./images/rule.png)

## Conclusion

Now you have learned how some queries in sparql function.

If you want to learn more about knowledge graphs, you can check out the [knowledge graph documentation](../knowledge_graph/README.md).

And if you haven't done the setup yet, you can check out the [setup guide](../setup/README.md).