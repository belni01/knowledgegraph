# Knowledge Graph

## Introduction

This read me contains general information about Knowledge Graphs. It explains what a knowledge graph is, what the
components are, etc.
To give the reader a basic understanding of our project.

## What is a Knowledge Graph

Basically, a knowledge graph is a way of organizing and displaying data.
In contrast to a relational database, which only stores the data, additional context is added in a knowledge
graph. This means that the data is not only stored, but also the information on how the data is related.
For example, Switzerland is a country on the continent of Europe.
A knowledge graph also has a flexible schema, which makes it possible to have multiple definitions of the data.  
A flexible schema also means that there are no fixed rules on how the data must be organized.  
For example, if you have a ‘Person’ node, this node can have several attributes such as name, age, city.
Changes to the attributes can be made if necessary without having to redefine the entire schema.

## Components of a Knowledge Graph

A knowledge graph basically consists of three components:

- Nodes
- Edges
- Labels

In this section, we explain each element.

### Nodes

A node is a logical representation of a real entity. For example, people, places, products, events,
etc. can be represented.

### Edges

Edges are directed representations of connections or relationships between the nodes.
You can represent various relationships, for example, ‘Valais is a part of Switzerland’.
Each edge has a direction that defines the relationship.
However, several relationships can also exist between two nodes.

### Labels

Labels are attributes that define the relationship between two nodes.

The following image shows the three components as an example:

![components knowledge graph](./images/Components.png)

- **Green**: Nodes which represent a real entity (post and user)
- **Red**: Inside the red square or with at the red arrow you find the edges, which represent the connections between
  the two nodes.
- **Yellow**: The labels to define the relationship.

Which means we have the following examples:

- A post can tag a user
- A user can like a post
- A post is created by a user
- A user can follow another user

## The graph data structure

To better understand the graph data structure, we can compare it with a relational concept. In a relational concept, you
have fixed data definitions from which you can then work out the schema. Whereby the schema is responsible for deciding
which data can enter the system or not.
In contrast, the graph data structure focuses on emphasizing the relationships between entities. In a graph, the data is
expressed as a ‘triple’. A ‘triple’ is two nodes connected by a relationship, i.e. an edge. To take the examples from
the previous paragraph: A user likes a post. This is represented by two nodes: "User" and "Post" and an Edge, "like".
With these options, it is easy for graphs to add new information. By creating new triples, no new schema is required as
compared to relational concepts.

## What is "Reasoning"

*Reasoning* is used in our project. This section explains what *reasoning* is and uses an example to show where
*reasoning* is used in this project.

Reasoning refers to the process of automatically inferring new information from existing data.
Below is a simple example:

Without reasoning, the situation is as follows. Berta and Klaus are mutual friends on a social media platform. As
reasoning is not used, Klaus and Tom are accepted as mutual followers and displayed accordingly:

![without reasoning](./images/without_reasoning.png)

If you do a query about all the friends of Klaus it will give only Berta as a friend of Klaus.

![with reasoning](./images/with_reasoning.png)

If Reasoning is now activated and used, the system can deduce that these two people must also be friends due to the
mutual following of Klaus and Tom. If you now use this in a query in which all of Klaus'
friends are displayed, not only Berta would be displayed, but also Tom. **Important!** Even if Tom is now considered as
a friend of Klaus because of the reasoning, the edge and the label
with 'follow' stays. So if you do a query 'who are the follower of Klaus' you will still receive Tom as answer.

But to make this work, you need to specify the reasoning rules, the database itself can't make them up.
If you wan't to learn, how to do this, you can check out the [implementation documentation](../implementation/README.md).

## RDF Graph data model

Since Stardog is used in this project, we work with graph data models based on RDF (Resource Description Framework).
RDF is a W3C standard which is used to exchange graph data.
The data model of Stardog is a directed semantic graph. Which means that the edge between two nodes is in a certain
direction (directed) and the data has a meaning to understand the relationship between elements (semantic).
As explained in the chapter **The graph data structure** Stardog works with triples and thus with nodes which are
connected with edges in one direction.

The following image shows a rdf graph based on the person Carlos Contoh:

![rdf example Carlos Contoh](./images/rdf_graph_CarlosContoh.png)

### RDF Nodes

A distinction is made between three different RDF nodes:

**Internationalized Resource Identifier (IRI)**

An IRI is used to uniquely identify the nodes and edges.
In the image above, the IRI of Carlos would be `http://safetynet.com/safetysphere/user/2` you can't see it,
because in this project `rdfs:labels` were used, to make the graphical representation easier to read.

**Blank node**

Node that doesn't have identifiers visible to a user. Blank nodes are used when the node doesn't need to be directly
referenced.

**Literal**

Concrete values to represent data types. These can be names (strings), numbers, dates, etc. For example, a literal could
be the city of the person Carlos Contoh. Then, as a node, you might have "Visp."

### RDF syntax and code explanations

To serialize the graph for exchanging data between two systems or for loading data into a system like Stardog, there are
various syntaxes available. In this project, the Turtle syntax was used.

The following code is a snippet from the file post_data.ttl.

```turtle
@prefix : <tag:stardog:designer:SafetySphere:model:> .
@prefix rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix xs:   <http://www.w3.org/2001/XMLSchema#> .


<http://safetynet.com/safetysphere/post/1>  rdf:type :Post ;
                                            :content "Today is a sunny day, perfect for a stroll in the park." ;
                                            :timestamp "2024-04-23T08:00:00" ;
                                            :title "Sunny Day in the Park" ;
                                            rdfs:label "Post 1" .
<http://safetynet.com/safetysphere/post/2>  rdf:type :Post ;
                                            :content "I just finished reading the book 'The Art of War.' Highly recommended!" ;
                                            :timestamp "2024-04-22T10:30:00" ;
                                            :title "Book Recommendation: The Art of War" ;
                                            rdfs:label "Post 2" .
```

#### Prefixes

As you can see, there are four prefixes.

- The prefix `@prefix : <tag:stardog:designer:SafetySphere:model:>` is a prefix that is defined by the user and is a
  default namespace.
  This is used to identify all nodes and edges in the graph. This prefix is abbreviated with **:**. This means that the
  default namespace can be used with the **:** symbol.
- The prefix `@prefix rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>` shortens the namespace to the abbreviation
- **rdf**, which is the standard namespace for the RDF syntax.
- The prefix `@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>` shortens the namespace to the abbreviation **rdfs**,
  which is the standard namespace for RDF schemas. It is used to define extended concepts of RDF such as the **rdfs:label**.
- The prefix `@prefix xs: <http://www.w3.org/2001/XMLSchema#>` shortens the namespace to the abbreviation **xs**, which
  is the standard namespace for XML schema data types. It is used to define data types such as strings, numbers etc.

#### Serialization

We take the first post to explain how we define the triple.

- With `<http://safetynet.com/safetysphere/post/1>` we specify the IRI to define that we want to serialize the first
  post.
- With `rdf:type :Post` you give the type to the triple. In other words, you define which entity this triple belongs to.
  To make it easier to understand, you can also read `rdf:type` as `is a`. This would mean:
  **<http://safetynet.com/safetysphere/post/1> is a post**
- The attributes of the post are defined with :content, :timestamp and :title.
- Finally, use `rdfs:label` to define a human-readable name for the resource.

#### Connect user and posts

Below you can see a code excerpt from the file **relationship_post.ttl**:

````
<http://safetynet.com/safetysphere/post/1> :created_by <http://safetynet.com/safetysphere/user/1> .
````

In this code snippet, post 1 is linked to a user who created the post.
The IRI of the post and the user are used for this.

## Virtual graphs

Virtual graphs can be used to extend the graph using existing external data sources without moving this data to the main
graph. This means that virtual graphs can be used to map relational data such as CSVs or data from NoSQL or SQL sources
such as MongoDB to RDF. Each virtual graph must have a unique name in order to refer to it and identify it.

A virtual graph consists of 4 components:

1. A unique name
2. a (external) data source
3. a properties file to specify configuration options
4. a data mapping file

To learn more about how virtual graphs are used in this project, visit our [implementation documentation](../implementation/README.md).
If you need help to set up a virtual graph, visit our [azure guide](../azure/README.md) and our [setup guide](../setup/README.md).

## Conclusion

This readme explains the basics of knowledge graphs and shows individual code snippets to give you a better
understanding of how to proceed in the project.

If you want to learn more about our project, read the following [documentation](../implementation/README.md).

If you want to recreate the project yourself, read our [setup guide](../setup/README.md).