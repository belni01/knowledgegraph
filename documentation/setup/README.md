# Setup Guide

## Dependencies

- Azure subscription
- Node
- Python
- Any tool to interface with an SQL server, like DataGrip or any other tool from JetBrains.

## Introduction

In this guide, you will learn how you can create a database on stardog, fill it with example data and how to start
an application, which communicates with your database.
If you want to learn how this project works, you can check out the [project documentation](../implementation/README.md).

## Stardog setup

### Create stardog account

In this project, we use cloud stardog as our database. You can create a free account on the
[stardog website](https://www.stardog.com/). You will have to fill in some information after the account creation.
Once this is done, you should be on this website.
**You need to verify your email address, before you can start using the database.**

Click on the bottom right on **New Stardog Cloud Endpoint**.

![account_creation](./images/account_creation.png)

Click on **Get Stardog Free**.

![select_plan](./images/select_plan.png)

Accept the terms of service and click on **Checkout**.

![checkout](./images/checkout.png)

Now you are ready to use stardog.

### Create database on Stardog

1. Open the **Stardog Designer** on your [cloud endpoint](https://cloud.stardog.com/u/0/).
2. Click on the top right on the arrow besides **New** and click on **Import project**.
3. Select the file **[SafetySphere.stardogdesigner](./resources/SafetySphere.stardogdesigner)**.

It should look like this:
![designer](./images/designer.png)

4. Click in the top right corner on **Publish**.
5. Click on **Select or create** in the database section. You need to give it the name **safetySphere**.
6. Click on **Create "safetySphere"**.

![create_db](./images/create_db.png)

7. Make sure that the IRI equals: **tag:stardog:designer:SafetySphere:model:**
8. Click on **Next** and **Publish** without changing anything else.
9. Open the **Stardog Explorer** to verify the creation.


### Virtual Graph connection

1. Create an SQL database on Azure or any other provider. The database name needs to be **SafetySphere**.
   <br/>
   If you don't know how to create a database on azure, you can follow the [Azure guide](../azure/README.md).
   <br/>
   **Disclaimer: If you use any other provider or of non-Microsoft SQL Server type, we can't guarantee that
   everything will work.**
2. If you are on Azure, make sure that the database can be reached by external services.
3. Open your SQL tool
4. Connect to the Azure database
5. Run the following commands

```tsql
create table dbo.Person
(
    id        int
        constraint Person_pk
            primary key,
    firstname nvarchar(25) not null,
    lastname  nvarchar(25) not null,
    city      nvarchar(25) not null
)
go

insert into dbo.Person (id, firstname, lastname, city)
values (14, N'Felix', N'Fischer', N'Freienbach'),
       (15, N'Lara', N'Lang', N'Lausanne'),
       (16, N'Simon', N'Stein', N'Schaffhausen');
```

6. Open **Stardog Studio** on your [cloud endpoint](https://cloud.stardog.com/u/0/).
7. In the sidebar, click on **Virtual Graphs**.
8. Click on the plus icon in the top right corner of the virtual graph box.
9. Set the *Virtual Graph Name* to **azure-stardog**. (The name needs to be exactly the same).
10. Select **New Data Source**.
11. Set the *Data Source Name* to **azure-stardog-database**.
12. Set the *Data Source Type* to **Microsoft SQL Server**.
13. Enter your *JDBC Connection URL*, make sure to include the name of the database
    <br/>
    ex: jdbc:sqlserver://<YOUR_URL>:1433;**database=SafetySphere;**
14. Enter your username and password.
15. In the bottom right corner disable **Auto-generate mapping**.
16. Click on **Browse** and select the file [mapping.sms](./resources/mapping.sms).
17. Click on **Create**.

### Create application user

1. Open **Stardog Studio** on your [cloud endpoint](https://cloud.stardog.com/u/0/).
2. Navigate to the security section (the last entry in the sidebar).
3. Click on the plus icon in the top right corner of the user box.
4. Create a user, the name and the password don't matter. Note the name and password, as you will need them later.
5. Click on your newly created user.
6. Click on **Add Explicit Permission**.
7. Select **ALL** as *Action*, **DB** as *Resource Type* and **safetySphere** as *Resources*.
8. Click on **Add**.

### Get Stardog Cloud endpoint url

1. Click on **Stardog Free** on your [cloud endpoint](https://cloud.stardog.com/u/0/) and select **Manage Endpoints**.

![manage_endpoints](./images/manage_endpoints.png)

2. Copy your **Endpoint URL**, you will need it later.

## Insert data

### Stardog

1. Open the file [constants.py](../../setup/helper/constants.py).
2. Set the username, password, and endpoint according to your settings.
3. Open a terminal in the [setup folder](../../setup).
4. Run the following commands

```powershell
python -m venv venv
venv\Scripts\activate
pip install -r requirements.txt
python main.py
```

## Start the React application

1. Open the file [constants.js](../../frontend/src/helpers/constants.js)
2. Set the username, password, and endpoint according to your settings.
3. Open a terminal in the [frontend folder](../../frontend).
4. Run the following commands

```powershell
npm install
npm run start
```

## Conclusion

Everything is now setup correctly and can be used. You can explore the example data in an application, which interfaces
with the stardog endpoint.

If you want to understand how the project works,
then you should check out the [project documentation](../implementation/README.md).

And if you don't understand what Knowledge Graphs are,
then you should check out the [Knowledge Graph documentation](../knowledge_graph/README.md)