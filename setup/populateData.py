import stardog
from helper.constants import conn_details
from pathlib import Path



with stardog.Connection('safetySphere', **conn_details) as conn:
    def add(file):
        path = str(Path(__file__).parent.resolve() / f'resource/{file}')
        conn.begin()
        conn.add(stardog.content.File(path))
        conn.commit()

    def remove(file):
        path = str(Path(__file__).parent.resolve() / f'resource/{file}')
        conn.begin()
        conn.remove(stardog.content.File(path))
        conn.commit()
    def read_user():
        conn.begin()
        print(conn.select('prefix : <tag:stardog:designer:SafetySphere:model:> SELECT * {?person a :Person ;rdfs:label ?fullname}'))