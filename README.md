# Knowledge Graph

## Group members
- Badertscher Janis
- Bellwald Nicolas
- Kostanjevec Ivan

## Introduction
Our project is to discover knowledge graphs and their benefits using Stardog. This includes discovering the possibilities that Stardog offers in this area. 
We deal with knowledge graphs in general, with different SPARQL queries and explore the connection of virtual graphs. 
By creating a small social media website called *SafetySphere*, we want to utilise this technology as much as the scope of the project allows.

### Structure of our project
You will find various folders in our repository. These are organised as follows:

#### React application
- frontend

#### Python code (populate data) and turtle files
- setup

#### Resources to setup the project
- documentation/setup/resources

#### Documentation (setup guide, knowledge graph, implementation)
- documentation

The documentation folder contains the following subfolders with the corresponding ReadMe files:
- [setup guide](./documentation/setup/README.md): Explanation how to set up the project to recreate it.
- [azure sql guide](./documentation/azure/README.md): Explanation how to set up a SQL database on azure for virtual graphs.
- [implementation](./documentation/implementation/README.md): Information on how our project is structured and how it works, as well as information on SPARQL queries.
- [knowledge graph](./documentation/knowledge_graph/README.md): Basic information about knowledge graphs.

## Technologies

### Stardog
Stardog is a graph database and knowledge graph platform. With the help of Stardog, it is possible to merge structured and semi-structured data from 
different data sources and display them in knowledge graphs.

Stardog was the main technology in our project. Our knowledge graph was created in Stardog, an external data source 
was integrated and the SPARQL queries were created and tested.

### Azure
Azure is a cloud computing service created by Microsoft for building, testing, deploying, and managing applications and
services through Microsoft-managed data centers. It provides software as a service (SaaS), platform as a service (PaaS)
and infrastructure as a service (IaaS) and supports many different programming languages, tools, and frameworks,
including both Microsoft-specific and third-party software and systems

In our project, we use Azure to host a database and explore the possibilities of virtual graphs.

### React
React.js, also known as React, is a free, open-source JavaScript library that is used to design web-based user interfaces.

In this project, React was used to develop the user interface of our social media application *SafetySphere*, but also to display the data from the Knowledge Graph in our application.

## Setup
To recreate our project, we recommend that you read the [setup guide](./documentation/setup/README.md). This contains step-by-step instructions from creating an account on Stardog to running the React application.

## Alternatives

### Neo4j
Neo4j is an alternative developed by Neo Technology. Neo4j is a popular graph database management system that can also be used to create knowledge graphs. 
What are the differences? Unlike Stardog, it does not use SPARQL for queries, but Cypher. The integration of virtual graphs is also not so simple. 
Unlike Stardog, Neo4j requires the integration of external data sources via plugins or user-defined extensions. 

### GraphDB
GraphDB from the company Ontotext is another alternative to Stardog for developing knowledge graphs. Unlike Stardog, however, GraphDB does not offer a web-based solution. 
GraphDB must first be installed locally. However, there are also options to create a GraphDB environment directly in Microsoft Azure, AWS cloud etc. 
Similarities to Stardog are that it also works with SPARQL and the graph data models are based on RDF.